import torch
from torch import nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from .utils import random_embedding

class CharBiLSTM(nn.Module):



    def __init__(self, config):
        super(CharBiLSTM, self).__init__()
        # self.embedding = layerUtil.getCharEmbeddingParameter()
        self.char_emb_size = config.char_emb_size
        self.char_lstm_hidden_size = config.char_lstm_hidden_size
        self.embedding = nn.Embedding.from_pretrained(random_embedding(config.num_char, self.char_emb_size),
                                                      freeze=False)
        self.rnn = nn.LSTM(self.char_emb_size, self.char_lstm_hidden_size, bidirectional=False,
                           batch_first=True).to(config.device)

        ##No dropout for character embedding

    def forward(self, seqTensors, seqLengths):
        """
        :param seqTensors: [batch_size, word_length]
        :param seqLengths: (batch_size, 1)
        Note it only accepts ordered (length) variable, length size is recorded in seq_lengths
        :return:
        """
        batchSize = seqTensors.shape[0]
        sentLength = seqTensors.shape[1]
        # seqTensors = seqTensors.view(batchSize * sentLength, -1)
        # seqLengths = seqLengths.view(batchSize * sentLength)

        packedChars = pack_padded_sequence(seqTensors, seqLengths, True)

        # sortedSeqLengths, permIdx = seqLengths.sort(0, descending=True)
        # _, recoverPermIdx = permIdx.sort(0, descending=False)
        # sortedSeqTensors = seqTensors[permIdx]


        lstmt_out, lstm_hidden = self.rnn(packedChars, None)
        # lstmOut, _ = pad_packed_sequence(lstmOut, batch_first=True)


        return lstm_hidden[0].transpose(1,0).contiguous.view(batchSize, -1)
