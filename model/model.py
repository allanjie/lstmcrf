

import torch
import torch.nn as nn
import torch.nn.functional as F
from .lstm_layer import Word_LSTM_Layer
from .crf import CRF

class NERModel(nn.Module):

    def __init__(self, config):
        super(NERModel, self).__init__()

        print("building the model")

        self.bilstm = Word_LSTM_Layer(config)
        self.crf = CRF(config)
        self.device = config.device

    def neg_log_likelihood_loss(self, word_inputs, word_seq_lengths, char_inputs, char_seq_lengths, batch_labels, mask):

        # torch.set_grad_enabled(True)
        word_rep = self.bilstm(word_inputs, word_seq_lengths, char_inputs, char_seq_lengths)
        batch_size = word_inputs.size(0)

        total_score, scores = self.crf(word_rep, word_seq_lengths, mask)
        gold_score = self.crf.scoreSentence(batch_labels, word_seq_lengths, scores, mask)
        return total_score - gold_score


    def forward(self, word_inputs, word_seq_lengths, char_inputs, char_seq_lengths, mask):
        word_rep = self.bilstm(word_inputs, word_seq_lengths, char_inputs, char_seq_lengths)
        best_scores, decode_idxs = self.crf.viterbiDecode(self.device, word_rep, mask)
        return best_scores, decode_idxs
