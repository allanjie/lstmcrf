
import torch
from torch import nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

class CharRNN(nn.Module):
    def __init__(self, config):
        super(CharRNN, self).__init__()
        self.embedding = nn.Embedding(config.num_char, config.char_emb_size).to(config.device)
        char_hidden_dim = config.char_hidden_dim // 2
        self.rnn = nn.LSTM(config.char_emb_size, char_hidden_dim, bidirectional=True).to(config.device)
        self.dropout = nn.Dropout(config.dropout).to(config.device)


    def forward(self, char_seq_tensors, char_seq_lengths):
        """
            input:
                input: Variable(batch_size, word_length)
                seq_lengths: numpy array (batch_size,  1)
            output:
                Variable(batch_size,, char_hidden_dim)
            Note it only accepts o  rdered (length) variable, length size is recorded in seq_lengths
        """
        # print(char_seq_tensors.size())
        # print("before char rnn forward")
        # print(char_seq_lengths)
        batch_size = char_seq_tensors.shape[0]

        sorted_seq_lens, permIdx = char_seq_lengths.sort(0, descending=True)
        _, recover_idx = permIdx.sort(0, descending=False)
        sorted_seq_tensors = char_seq_tensors[permIdx]
        # print("inside char rnn")
        # print(sorted_seq_lens)
        # print(sorted_seq_tensors)

        seq_embedding = self.dropout(self.embedding(sorted_seq_tensors))
        packed_chars = pack_padded_sequence(seq_embedding, sorted_seq_lens, batch_first=True)
        rnn_output, (hn, _) = self.rnn(packed_chars, None)
        lstmOut, _ = pad_packed_sequence(rnn_output, batch_first=True)

        lstmOut = lstmOut[recover_idx]
        # print(lstmOut[17])
        ## take the last hidden state.
        return hn.transpose(0,1)[recover_idx].contiguous().view(batch_size, -1)
