# 
# @author: Allan
#

import torch
from torch import nn
from model.char_rnn import CharRNN
import numpy as np

class CharWordEmbedding(nn.Module):
    def __init__(self, config):
        super(CharWordEmbedding, self).__init__()
        self.embedding = nn.Embedding.from_pretrained(torch.FloatTensor(config.word_embedding), freeze=False).to(
            config.device)
        self.use_char = config.use_char
        if config.use_char:
            self.charEmbedding = CharRNN(config)
        self.dropout = nn.Dropout(config.dropout).to(config.device)

    def forward(self, word_seq_tensors, char_seq_tensors, char_seq_lens):
        """
            input:
                word_seq_tensors: (batch_size, sent_len)
                char_seq_tensors: (batch_size*sent_len, word_length)
                char_seq_lens: list of whole batch_size for char, (batch_size*sent_len, 1)
            output:
                Variable(batch_size, sent_len, hidden_dim)
        """
        batch_size = word_seq_tensors.size(0)
        sent_len = word_seq_tensors.size(1)
        wordEmbedding = self.embedding(word_seq_tensors)
        if self.use_char:
            charCNNEmbedding = self.charEmbedding(char_seq_tensors, char_seq_lens).view(batch_size,sent_len,-1)
            mergeEmbedding = torch.cat([wordEmbedding, charCNNEmbedding], 2)
        else:
            mergeEmbedding = wordEmbedding
        wordEmbeddingDropOut = self.dropout(mergeEmbedding)
        return wordEmbeddingDropOut
