# 
# @author: Allan
#

from torch import nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from model.char_and_word import CharWordEmbedding

"""
  Word LSTM layer with linear layer to the CRF as well.
    
"""
class Word_LSTM_Layer(nn.Module):
    def __init__(self, config):
        super(Word_LSTM_Layer, self).__init__()
        self.hidden_dim = config.hidden_dim
        self.use_brnn = config.use_brnn
        self.label_size = config.label_size
        if config.use_brnn:
            self.hidden_dim = self.hidden_dim // 2
        ### input to lstm shoule be (batch, seq, feature)

        self.input_size = config.embedding_dim
        if config.use_char:
            self.input_size += config.char_hidden_dim
        self.rnn = nn.LSTM(self.input_size, self.hidden_dim, config.num_layers,
                           bidirectional=self.use_brnn, batch_first=True).to(config.device)
        self.lstm_dropout = nn.Dropout(config.dropout).to(config.device)
        self.hidden2tag = nn.Linear(config.hidden_dim, self.label_size).to(config.device)
        self.wordrep = CharWordEmbedding(config)

    def forward(self, word_seq_tensors, word_seq_lengths, char_seq_tensors, char_seq_lengths):
        """
            input:
                inputs: (batch_size, sent_len)
                inputSeqLengths: list of batch_size, (batch_size,1)
                char_inputs: (batch_size*sent_len, word_length)
                char_seq_lengths: list of whole batch_size for char, (batch_size*sent_len, 1)
                char_seq_recover: variable which records the char order information, used to recover char order
            output:
                Variable(batch_size, sent_len, hidden_dim)
        """
        word_represent = self.wordrep(word_seq_tensors, char_seq_tensors, char_seq_lengths)
        # print(word_represent.size())
        packedWords = pack_padded_sequence(word_represent, word_seq_lengths, batch_first=True)
        hidden = None
        lstmOut, hidden = self.rnn(packedWords, hidden)
        lstmOut, _ = pad_packed_sequence(lstmOut, batch_first=True)
        # print(lstmOut.size())
        featureOut = self.lstm_dropout(lstmOut)
        # print(featureOut.size())
        linearOutput = self.hidden2tag(featureOut)
        # print(linearOutput.size())
        return linearOutput