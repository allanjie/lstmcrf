# 
# @author: Allan
#


import torch

def logSumExp(vec):
    """

    :param vec: [batchSize * tagSize * tagSize]
    :return: [batchSize * tagSize]
    """
    maxScores, idx = torch.max(vec, 2)
    maxScores[maxScores == -float("Inf")] = 0
    maxScoresExpanded = maxScores.view(vec.shape[0], vec.shape[1], 1).expand(vec.shape[0], vec.shape[1], vec.shape[2])
    return maxScores + torch.log(torch.sum(torch.exp(vec - maxScoresExpanded), 2))
