import torch
import numpy as np

def batchify_with_label(input_batch_list, config):
    """
        input: list of words, chars and labels, various length. [[words,chars, labels],[words,chars,labels],...]
            words: word ids for one sentence. (batch_size, sent_len)
            chars: char ids for on sentences, various length. (batch_size, sent_len, each_word_length)
        output:
            zero padding for word and char, with their batch length
            word_seq_tensor: (batch_size, max_sent_len) Variable
            word_seq_lengths: (batch_size,1) Tensor
            char_seq_tensor: (batch_size*max_sent_len, max_word_len) Variable
            char_seq_lengths: (batch_size*max_sent_len,1) Tensor
            char_seq_recover: (batch_size*max_sent_len,1)  recover char sequence order
            label_seq_tensor: (batch_size, max_sent_len)
            mask: (batch_size, max_sent_len)
    """
    # torch.set_grad_enabled(False)
    batch_size = len(input_batch_list)
    words = [sent[0] for sent in input_batch_list]
    # features = [np.asarray(sent[1]) for sent in input_batch_list]
    # feature_num = len(features[0][0])
    chars = [sent[1] for sent in input_batch_list]
    labels = [sent[2] for sent in input_batch_list]
    word_seq_lengths = torch.LongTensor(list(map(len, words)))
    max_seq_len = word_seq_lengths.max().item()
    # print(max_seq_len)
    word_seq_tensor = torch.zeros((batch_size, max_seq_len)).long()
    label_seq_tensor = torch.zeros((batch_size, max_seq_len)).long()
    # feature_seq_tensors = []
    # for idx in range(feature_num):
    #     feature_seq_tensors.append(torch.zeros((batch_size, max_seq_len)).long())
    mask = torch.zeros((batch_size, max_seq_len)).byte()
    for idx, (seq, label, seqlen) in enumerate(zip(words, labels, word_seq_lengths)):
        word_seq_tensor[idx, :seqlen] = torch.LongTensor(seq)
        label_seq_tensor[idx, :seqlen] = torch.LongTensor(label)
        # print(seqlen)
        mask[idx, :seqlen] = torch.Tensor([1]*seqlen.item())
        # for idy in range(feature_num):
        #     feature_seq_tensors[idy][idx,:seqlen] = torch.LongTensor(features[idx][:,idy])
    word_seq_lengths, word_perm_idx = word_seq_lengths.sort(0, descending=True)
    word_seq_tensor = word_seq_tensor[word_perm_idx]
    # for idx in range(feature_num):
    #     feature_seq_tensors[idx] = feature_seq_tensors[idx][word_perm_idx]

    label_seq_tensor = label_seq_tensor[word_perm_idx]
    mask = mask[word_perm_idx]
    ### deal with char
    # pad_chars (batch_size, max_seq_len)
    pad_chars = [chars[idx] + [[0]] * (max_seq_len-len(chars[idx])) for idx in range(len(chars))]
    length_list = [list(map(len, pad_char)) for pad_char in pad_chars]
    max_word_len = max(map(max, length_list))
    char_seq_tensor = torch.zeros((batch_size, max_seq_len, max_word_len)).long()
    char_seq_lengths = torch.LongTensor(length_list)
    for idx, (seq, seqlen) in enumerate(zip(pad_chars, char_seq_lengths)):
        for idy, (word, wordlen) in enumerate(zip(seq, seqlen)):
            # print len(word), wordlen
            char_seq_tensor[idx, idy, :wordlen] = torch.LongTensor(word)
    char_seq_tensor = char_seq_tensor[word_perm_idx].view(batch_size*max_seq_len,-1)
    char_seq_lengths = char_seq_lengths[word_perm_idx].view(batch_size*max_seq_len,)

    # char_seq_lengths, char_perm_idx = char_seq_lengths.sort(0, descending=True)
    # char_seq_tensor = char_seq_tensor[char_perm_idx]

    # _, char_seq_recover = char_perm_idx.sort(0, descending=False)
    # _, word_seq_recover = word_perm_idx.sort(0, descending=False)

    word_seq_tensor = word_seq_tensor.to(config.device)
    # for idx in range(feature_num):
    #     feature_seq_tensors[idx] = feature_seq_tensors[idx].to(config.device)
    word_seq_lengths = word_seq_lengths.to(config.device)
    # word_seq_recover = word_seq_recover.to(config.device)
    label_seq_tensor = label_seq_tensor.to(config.device)
    char_seq_tensor = char_seq_tensor.to(config.device)
    # char_seq_recover = char_seq_recover.to(config.device)
    mask = mask.to(config.device)
    # return word_seq_tensor, word_seq_lengths, word_seq_recover, char_seq_tensor, char_seq_lengths, char_seq_recover, label_seq_tensor, mask
    # print("batching")
    # print(char_seq_lengths)
    return word_seq_tensor, word_seq_lengths, char_seq_tensor, char_seq_lengths, label_seq_tensor, mask

