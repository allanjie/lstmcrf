
from .batch_op import batchify_with_label

def parseTag(s):
    if s != '<UNK>' and s != '<START>' and s != '<END>' and s != '<PAD>':
        if s == 'O':
            return ('O', '<UNK>')
        return (s.split('-'))
    else:
        return ('<UNK>', '<UNK>')

def get_entities(label_seq, idx2Tag):
    entityStart = -1
    entities = []
    current = 0
    while current < len(label_seq):
        currentLabel, currentType = parseTag(idx2Tag[label_seq[current]])
        if currentLabel.startswith('O'):
            if entityStart != -1:
                entities.append((entityStart, current - 1, idx2Tag[label_seq[entityStart]].split('-')[1]))
                entityStart = -1
        elif currentLabel.startswith('B'):
            if entityStart != -1:
                entities.append((entityStart, current - 1, idx2Tag[label_seq[entityStart]].split('-')[1]))
            entityStart = current
        elif currentLabel.startswith('S'):
            if entityStart != -1:
                entities.append((entityStart, current - 1, idx2Tag[label_seq[entityStart]].split('-')[1]))
            entities.append((current, current, currentType))
            entityStart = -1
        elif currentLabel.startswith('E'):
            if entityStart != -1:
                entities.append((entityStart, current, currentType))
            entityStart = -1
        current += 1

    if entityStart != -1:
        entities.append((entityStart, len(label_seq) - 1, idx2Tag[label_seq[entityStart]].split('-')[1]))
    return entities

def evaluate(config, model, insts_ids):
    batch_size = config.batch_size
    insts_num = len(insts_ids)
    total_batch = insts_num//batch_size + 1
    total_predict = 0
    total_gold = 0
    total_p = 0
    for batch_id in range(total_batch):
        start = batch_id * batch_size
        end = (batch_id + 1) * batch_size
        if end > insts_num:
            end = insts_num
        insts = insts_ids[start:end]
        if not insts:
            continue
        batch_word, batch_wordlen, batch_char, batch_charlen, batch_label, mask = batchify_with_label(
            insts, config)
        best_scores, label_seqs = model(batch_word, batch_wordlen, batch_char, batch_charlen, mask)

        batch_size = batch_word.size(0)

        for idx in range(batch_size):
            sent_len = batch_wordlen[idx]
            pred = label_seqs[idx][: sent_len].tolist()
            gold = batch_label[idx][:sent_len]
            print("gold")
            print(gold)
            print("prediction")
            print(pred)
            pred.reverse()

            gold_entities = get_entities(gold, config.idx2labels)
            pred_entities = get_entities(pred, config.idx2labels)

            total_predict += len(pred_entities)
            total_gold += len(gold_entities)

            if len(pred_entities) == 0:
                continue

            pred_idx = 0
            for entity_idx in range(len(gold_entities)):
                # strict_match = False
                entity_start, entity_end, entity_type = gold_entities[entity_idx]
                while pred_idx < len(pred_entities) - 1 and pred_entities[pred_idx][1] < entity_start:
                    pred_idx += 1
                if entity_type == pred_entities[pred_idx][2]:
                    if entity_start == pred_entities[pred_idx][0] and entity_end == pred_entities[pred_idx][1]:
                        total_p += 1
                        # strict_match = True
        precision = total_p / total_predict if total_p > 0 else 0
        recall = total_p / total_gold if total_gold > 0 else 0
        f1 = 2 * precision * recall / (precision + recall) if precision + recall > 0 else 0
        return precision * 100, recall * 100, f1 *100


        print(label_seqs.size())
        print(batch_label.size())
        print("###one batch")