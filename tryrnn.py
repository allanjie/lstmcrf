# 
# @author: Allan
#

import torch
import torch.nn as nn

rnn = nn.LSTM(3, 5, 1, bidirectional=True, batch_first=True)
input = torch.randn(2, 5, 3)
h0 = torch.randn(2, 2, 5)
c0 = torch.randn(2, 2, 5)
output, (hn, cn) = rnn(input, (h0, c0))

print(output)
print(hn.transpose(1,0).contiguous().view(2, -1))