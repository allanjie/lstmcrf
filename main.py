# 
# @author: Allan
#

import argparse
import torch
import random
import numpy as np
from config import  Config
from reader import Reader
from model.model import NERModel
import torch.optim as optim
import time

from utils.batch_op import batchify_with_label
from utils.evaluator import evaluate

def setSeed(seed, useGpu=False):
    torch.manual_seed(seed)
    random.seed(seed)
    np.random.seed(seed)
    if useGpu:
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)

def parse_arguments(parser):
    parser.add_argument('--mode', type=str, default='train')
    parser.add_argument('--gpu', action="store_true", default=False)
    parser.add_argument('--seed', type=int, default=1234)
    parser.add_argument('--digit2zero', action="store_true", default=True)
    parser.add_argument('--train_file', type=str, default="data/conll2003/train.txt")
    parser.add_argument('--dev_file', type=str, default="data/conll2003/dev.txt")
    parser.add_argument('--test_file', type=str, default="data/conll2003/test.txt")
    # parser.add_argument('--embedding_file', type=str, default="data/emb/glove.6B.100d.txt")
    parser.add_argument('--embedding_file', type=str, default=None)
    parser.add_argument('--embedding_dim', type=int, default=100)
    parser.add_argument('--optimizer', type=str, default="sgd")
    parser.add_argument('--learning_rate', type=float, default=0.015)
    parser.add_argument('--momentum', type=float, default=0.0)
    parser.add_argument('--l2', type=float, default=0.0)

    ##model hyperparameter
    parser.add_argument('--hidden_dim', type=int, default=100)

    return parser.parse_args()

def get_optimizer(config, model):
    optimizer = None
    if config.optimizer == "sgd":
        optimizer = optim.SGD(model.parameters(), lr=config.learning_rate, momentum=config.momentum, weight_decay=config.l2)
    else:
        print("unknown optimizer: " + config.optimizer)
        exit(1)
    return optimizer

def train(config, train_insts, dev_insts, test_insts):
    print("Initializing the model")
    model = NERModel(config)
    optimizer = get_optimizer(config, model)
    best_dev_fscore = -100

    print("Start training")
    ###start training
    train_insts_ids = config.map_insts_ids(train_insts)
    dev_insts_ids = config.map_insts_ids(dev_insts)
    test_insts_ids = config.map_insts_ids(test_insts)
    for idx in range(1, config.num_epochs + 1):
        epoch_start = time.time()
        print("Epoch: %s/%s" % (idx, config.num_epochs))
        if config.optimizer == "sgd":
            optimizer = lr_decay(optimizer, idx, config.lr_decay, config.learning_rate)

        model.train()
        model.zero_grad()
        batch_size = config.batch_size
        batch_id = 0
        train_num = len(train_insts_ids)
        total_batch = train_num // batch_size + 1

        epoch_loss = 0
        for batch_id in range(total_batch):
            start = batch_id * batch_size
            end = (batch_id + 1) * batch_size
            if end > train_num:
                end = train_num
            insts = train_insts_ids[start:end]
            # print(insts[0])
            if not insts:
                continue
            batch_word, batch_wordlen, batch_char, batch_charlen, batch_label, mask = batchify_with_label(
                insts, config)

            # print("main function")
            # print(len(batch_word))
            # print(batch_word)
            # print(batch_wordlen)
            # print(batch_label)
            # print(mask)
            # print(len(batch_char))
            # print(batch_char)
            loss  = model.neg_log_likelihood_loss(batch_word, batch_wordlen, batch_char, batch_charlen, batch_label, mask)
            epoch_loss += loss.item()
            # print(" batch loss: " + str(loss.item() ))
            loss.backward()
            optimizer.step()
            model.zero_grad()
            # exit(0)
        if config.use_dev:
            model.eval()
            p, r, f1 = evaluate(config, model, dev_insts_ids)
            print(p, " ", r, " " ,f1)
        print("epoch : " + str(epoch_loss))


def lr_decay(optimizer, epoch, decay_rate, init_lr):
    lr = init_lr / (1 + decay_rate * epoch)
    print(" Learning rate is setted as:", lr)
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr
    return optimizer

if __name__=="__main__":

    parser = argparse.ArgumentParser(description="LSTM CRF implementation")
    opt = parse_arguments(parser)
    config = Config(opt)

    reader = Reader(config.digit2zero)
    setSeed(config.seed, opt.gpu)

    train_insts = reader.read_from_file(config.train_file, config.train_num)
    dev_insts = reader.read_from_file(config.dev_file, config.dev_num)
    test_insts = reader.read_from_file(config.test_file, config.test_num)
    print(reader.all_vocab)

    config.build_emb_table(reader.all_vocab)

    config.use_iobes(train_insts)
    config.use_iobes(dev_insts)
    config.use_iobes(test_insts)
    config.build_label_idx(train_insts)
    config.build_char_idx(train_insts)
    config.build_char_idx(dev_insts)
    config.build_char_idx(test_insts)

    print("num chars: " + str(config.num_char))
    print(str(config.char2idx))

    print("num words: " + str(len(config.word2idx)))
    print(config.word2idx)



    train(config, train_insts, dev_insts, test_insts)

    print(opt.mode)